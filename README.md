## What is this project for? ##

Keep It Simple Stupid implementation of [Cache](http://en.wikipedia.org/wiki/Database_caching). 
The purpose of the project is to explore simple in-memory cache implementations in Java language (without using 3rd party libraries or frameworks). 

_Requirements:_

* The TTL of the items is fairly short (~10 seconds);
* The system has plenty of memory;
* The keys are widely distributed across the keyspace;
* Each item may or may not be accessed during the TTL;
* Each item might be accessed multiple times during the TTL.

Version 0.0.1 SNAPSHOT

### License ###
It is licensed under an MIT-style permissive [license](http://www.opensource.org/licenses/mit-license.php).

## How do I get set up? ##

### Set up: ###
* Install [JDK 1.7+](http://docs.oracle.com/javase/7/docs/webnotes/install/);
* Set-up [Maven 3](http://maven.apache.org/download.cgi);
* Download [kisscache zip](https://bitbucket.org/pranasb/kisscache/get/master.zip); 
* Extract to selected work directory e.g. `C:\User\<User Name>\projects` in Windows or `~/projects` for Linux;
* Navigate to extracted source package `cd pranasb-kisscache-*`;
* Build `mvn package`;
* Store binary distribution `target/kisscache-0.0.1-SNAPSHOT.jar` to dedicated location and/or artifact repository and use as library.

###  How to test: ###
* Unit test are executed during `mvn package`
* The `net.orangecap.util.TtlCache` usage resembles implementation of `java.util.HashMap`. It has constructors with additional `TTL` parameter and simplified `get(...)` and `put(...)` methods.
* See [net.oragecap.util.TtlCacheTest](https://bitbucket.org/pranasb/kisscache/src/325b6f5c2de784592c62ec81a5300c1d22f3e805/src/test/java/net/orangecap/util/TtlCacheTest.java?at=master) implementation for usage examples.

### Who do I talk to? ###
* Repository owner and administrator is [pranasb](https://bitbucket.org/pranasb), e-mail: pranas at OrangeCap dot Net

## Other similar projects: ##
* [Google Guava](https://code.google.com/p/guava-libraries/wiki/CachesExplained);
* [Apache Commons JCS](http://commons.apache.org/proper/commons-jcs/);
* [Pivotal GemFire](http://gemfire.docs.gopivotal.com/latest/userguide/index.html);
* [Hazelcast](http://hazelcast.com/products/hazelcast/);
* [GridGain](http://www.gridgain.org/platform/data-grid/);
* [Terracotta](http://terracotta.org/) and sibling open source project [EHCACHE](http://ehcache.org/);
* [Coherence](http://www.oracle.com/technetwork/middleware/coherence/overview/index.html).