package net.orangecap.util;

import org.junit.Test;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * The Unit Test for {@link net.orangecap.util.TtlCache}.
 */
public class TtlCacheTest {

    /**
     * Ten minutes represented in milliseconds.
     */
    public static final long TEN_SECONDS = TimeUnit.SECONDS.toMillis(10);

    /**
     * Test case: All values must be evicted from the cache.
     */
    @Test
    public void testImmediateEviction() {
        // Entries cleaned up on each access
        final Map<Integer, Character> fixture = new TtlCache<>(-1L);
        fixture.put(1, 'A');
        fixture.put(2, 'B');
        assertNull(fixture.get(1));
        assertNull(fixture.get(2));
    }

    /**
     * Test case: All values must be preserved in the cache.
     */
    @Test
    public void testEvictionRetainedValues() {
        // Entries of such cache are cleaned up for each access.
        final Map<Integer, Character> fixture = new TtlCache<>(TEN_SECONDS);
        fixture.put(1, 'A');
        fixture.put(2, 'B');
        assertNotNull(fixture.get(1));
        assertNotNull(fixture.get(2));
    }

    /**
     * Test different ways to construct the cache.
     */
    @Test
    public void testConstructors() {
        testCacheHitAndMiss(new TtlCache<Integer, String>(TEN_SECONDS));
        testCacheHitAndMiss(new TtlCache<Integer, String>(TEN_SECONDS, 100));
        testCacheHitAndMiss(new TtlCache<Integer, String>(TEN_SECONDS, 10, 0.5f));
    }

    /**
     * Test one hit and one miss for the specified cache instance.
     *
     * @param cache the cache instance to check.
     */
    private void testCacheHitAndMiss(Map<Integer, String> cache) {
        cache.put(1, "ABC");
        assertNull(cache.get(2));
        assertEquals("ABC", cache.get(1));
    }
}