package net.orangecap.util;

import java.util.*;

/**
 * The cache automatically cleaning Cache based on TTL settings on access get and put invocations.
 * <p/>
 * The implementation is <bb>not thread safe<bb/>.
 */
public class TtlCache<K, V> implements Map<K, V> {
    /**
     * The default initial capacity.
     */
    static final int DEFAULT_INITIAL_CAPACITY = 16;

    /**
     * The default load factor.
     */
    static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     *  The first element index.
     */
    static final int FIRST_ELEMENT_INDEX = 0;

    /**
     * Mapping from key to value.
     */
    private final Map<K, V> map;

    /**
     * Time to live in milliseconds.
     */
    private final long ttl;

    /**
     * The container of active keys.
     */
    private final List<TimeStampedKey<K>> activeKeys;

    /**
     * Constructs an empty <tt>HashMap</tt> with the default initial capacity
     * (16) and the default load factor (0.75).
     *
     * @param ttl the time to live in milliseconds.
     */
    public TtlCache(final long ttl) {
        this(ttl, DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR);
    }

    /**
     * Constructs an empty <tt>TtlCache</tt> with the specified initial
     * capacity and the default load factor (0.75).
     *
     * @param ttl             the time to live in milliseconds.
     * @param initialCapacity the initial capacity.
     * @throws IllegalArgumentException if the initial capacity is negative.
     */
    public TtlCache(final long ttl, final int initialCapacity) throws IllegalArgumentException {
        this(ttl, initialCapacity, DEFAULT_LOAD_FACTOR);
    }

    /**
     * Constructs an empty <tt>TtlCache</tt> with the specified initial
     * capacity and load factor.
     *
     * @param ttl             the time to live in milliseconds.
     * @param initialCapacity the initial capacity
     * @param loadFactor      the load factor
     * @throws IllegalArgumentException if the initial capacity is negative
     *                                  or the load factor is nonpositive
     */
    public TtlCache(final long ttl, final int initialCapacity, final float loadFactor) throws IllegalArgumentException {
        this.ttl = ttl;
        this.map = new HashMap<>(initialCapacity, loadFactor);

        // Create Priority Queue for keeping entries based on creation time and
        this.activeKeys = new LinkedList<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        cleanup();
        return map.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        cleanup();
        return map.isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsKey(Object key) {
        cleanup();
        return map.containsKey(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsValue(Object value) {
        cleanup();
        return map.containsValue(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V put(final K key, final V value) {
        final TimeStampedKey<K> timeStampedKey = new TimeStampedKey<>(key, System.currentTimeMillis());
        cleanup();
        this.activeKeys.add(timeStampedKey);
        return this.map.put(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V remove(Object key) {
        this.activeKeys.remove(new TimeStampedKey<>((K) key, 0L));
        return this.map.remove(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        cleanup();
        for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
            final K key = entry.getKey();
            final V value = entry.getValue();
            this.put(key, value);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        this.activeKeys.clear();
        this.map.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<K> keySet() {
        return this.map.keySet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<V> values() {
        return this.map.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Entry<K, V>> entrySet() {
        return this.map.entrySet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V get(final Object key) {
        cleanup();
        return this.map.get(key);
    }

    /**
     * Remove internal container references from expired values.
     * The value can be removed by garbage collector once all hard references to keys are removed.
     */
    private void cleanup() {
        final long currentTime = System.currentTimeMillis();
        final long preserveTill = currentTime - this.ttl;

        // Get oldest (times stamped) keys and check if there are items for removal
        while (!this.activeKeys.isEmpty()) {
            final TimeStampedKey<K> timeStampedKey = this.activeKeys.get(FIRST_ELEMENT_INDEX);
            if (timeStampedKey.creationTime < preserveTill) {
                this.activeKeys.remove(FIRST_ELEMENT_INDEX);
                this.map.remove(timeStampedKey.key);
            } else {
                break;
            }
        }
    }

    /**
     * The class to hold time stamped key.
     */
    private static class TimeStampedKey<T> {
        /**
         * The key value.
         */
        private final T key;

        /**
         * The creation time for entries and keys.
         */
        private final long creationTime;

        /**
         * Create <tt>TimeStampedKey<tt/> holding key and timestamp.
         *
         * @param key       the key to tag with time stamp.
         * @param timeStamp the time stamp.
         */
        private TimeStampedKey(final T key, final long timeStamp) {
            this.creationTime = timeStamp;
            this.key = key;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString() {
            return String.format("TimeStampedKey[key=%s, creationTime=%s]", key, creationTime);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int hashCode() {
            // Delegate to key's hashCode
            return this.key.hashCode();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }

            if (!(o instanceof TimeStampedKey)) {
                return false;
            }

            // Compare key values only
            final TimeStampedKey oKey = (TimeStampedKey) o;
            return this.key.equals(oKey.key);
        }
    }
}